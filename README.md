# personalAccountPython

Use
<br> ```main-gui``` for stable
<br> ```main-gui-disabled``` for fallback

Main Gui
![](https://github.com/wwwxkz/personalAccountPython/blob/main/README/0.gif)
Main Gui Disabled
![](https://github.com/wwwxkz/personalAccountPython/blob/main/README/1.gif)
